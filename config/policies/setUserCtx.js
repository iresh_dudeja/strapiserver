'use strict';
global.fetch = require('node-fetch');
const _ = require('lodash');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');
global.navigator = () => null;
const userController=require('../../api/serviceuser/controllers/Serviceuser');

const config=require('../environments/development/AWScredentials.json');
const poolData = {
    UserPoolId: config["UserPoolId"],
    ClientId: config["ClientId"]
};

const jwt=require('jsonwebtoken');

module.exports=async(ctx,next)=>{

    var token=ctx.request.header['token'];
    var username= await decoceJWT(token);
    if(!username){
        ctx.unauthorized("Not a valid JWT Token!")
    }
    ctx.params.username=username;
    let user=await setUser(ctx);
    if(user==null){
        ctx.unauthorized("Not a valid user! Login or Register to get a valid user credentials.");
        return;
    }
    ctx.request.user=user;
    strapi.log.info("User context set for a given request : ",ctx.request.user);
    await next();
}

const decoceJWT=  async(token)=>{
    var decoded=jwt.decode(token,{complete:true});
   // console.log("Decoded JWT token ",decoded);
    if(!decoded){
        return null;
    }
   // console.log("Username: ",decoded.payload['username']);
    return decoded.payload['username'];
}

const setUser= async(ctx)=>{
     let user= await userController.findbyUsername(ctx);
     if(Array.isArray(user) && user.length==0){
         return null;
     }
     return user[0];
     
 }