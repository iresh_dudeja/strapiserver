'use strict';
global.fetch = require('node-fetch');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
global.navigator = () => null;
 const config=require('../environments/development/AWScredentials.json');
/**
 * `isAuthenticated` policy.
 */

module.exports = async (ctx, next) => { 
     var returnVal=await Validate(ctx.request.header['token']);
     //console.log("I am here..", typeof(returnVal));
     if(returnVal==null || typeof(returnVal)=='undefined'){
        strapi.log.error("Not a valid JWT Token!");
        ctx.unauthorized("Not a valid JWT Token!")
        return;
     }
     strapi.log.info("JWT Token Validation is successful! ");
     await next();
};

const Validate = async (token)=>{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
                if(token){
                            request({
                            url : config["RequestURL"],
                                json : true
                            }, function(error, response, body){
                                try{
                                if (!error && response.statusCode === 200) {
                                    let pems = {};
                                    var keys = body['keys'];
                                    for(var i = 0; i < keys.length; i++) {
                                        var key_id = keys[i].kid;
                                        var modulus = keys[i].n;
                                        var exponent = keys[i].e;
                                        var key_type = keys[i].kty;
                                        var jwk = { kty: key_type, n: modulus, e: exponent};
                                        var pem = jwkToPem(jwk);
                                        pems[key_id] = pem;
                                    }
                                    var decodedJwt =  jwt.decode(token, {complete: true});
                                        if (!decodedJwt) {
                                            resolve(null);
                                            
                                        }
                                        var kid = decodedJwt.header.kid;
                                        var pem = pems[kid];
                                            if (!pem) {
                                                resolve(null);       
                                            }
                                    jwt.verify(token, pem, function(err, payload) {
                                            if(err) {
                                                resolve(null);
                                            } else{
                                                resolve("success");
                                            }
                                        });
                                } else {
                                    resolve(null);
                                }
                            }catch(err){
                                strapi.log.error("Error while decoding JWT token:  ",err);
                                //console.log("Caught ");
                                resolve(null);
                            }
                            });
                       
                   }else{
                    resolve(null);
                   }
     
        }, 1000);
      
      });
       
  }