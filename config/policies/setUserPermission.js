
'use strict';
const userrole=require('../../api/userrole/controllers/Userrole');

module.exports=async(ctx,next)=>{

    var permission=await getRole(ctx);
    //console.log("Returned value: ",permission);
    if(permission===null || typeof(permission)=='undefined'){
        ctx.request.user.permission=null;
        ctx.log.info("User: ",ctx.request.user.Username," is set with no permissions");
        ctx.unauthorized("Not a valid user!");
    }
    else{ 
        ctx.request.user.permission=permission['Permissions'];
      //  if(ctx.request.user.permission.toUppercase()=='ADMINISTRATOR'|| ctx.request.user.permission.toUppercase()=='SUPERUSER'){
            ctx.log.info(" UserLoogged In : ",ctx.request.user.Username," Permissions: ",ctx.request.user.permission);
            await next();
            return;
      //  }
        
    }
    ctx.unauthorized("Not a valid user!");
    
}

const getRole=async(ctx)=>{
   return await userrole.getRole(ctx);

}





 