'use strict';

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {

    /**
   * Return UserRole Object.
   *
   * @return {Object}
   */
    getRole: async (ctx) => {
      strapi.log.info("In a Userrole content type getRole controller function: ",ctx.request);
      ctx.params._id= ctx.request.user.userrole;
      return  strapi.services.userrole.fetch(ctx.params);
    }

};
