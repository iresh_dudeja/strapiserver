'use strict';

/**
 * Read the documentation () to implement custom service functions
 */

const _ = require('lodash');
const { convertRestQueryParams, buildQuery } = require('strapi-utils');
const mongodb=require('mongodb');
module.exports = {


   /**
   * Promise to fetch a/an login.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    strapi.log.info("In a Userrole content type fetch service function: ",params);
    // Select field to populate.
    const populate = Userrole.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Userrole
      .findOne(_.pick(params, _.keys(Userrole.schema.paths)));
     // .populate(populate);
  }

};
