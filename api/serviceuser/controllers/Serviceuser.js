'use strict';


/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {

    findbyUsername: async (ctx) => {
        strapi.log.info("In a Serviceuser content type findbyUsername controller function: ",ctx.request);
        return strapi.services.serviceuser.findbyUsername({"Username":ctx.params.username});
     }

};
