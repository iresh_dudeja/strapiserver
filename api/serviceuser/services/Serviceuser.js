'use strict';

/**
 * Read the documentation () to implement custom service functions
 */
const _ = require('lodash');
const { convertRestQueryParams, buildQuery } = require('strapi-utils');

module.exports = {

  /**
   * Promise to fetch a/an ServiceUser.
   *
   * @return {Promise}
   */
    findbyUsername : async(params)=>{
      strapi.log.info("In a Serviceuser content type findbyUsername service function: ",params);
        const filters = convertRestQueryParams(params);
        // const populateOpt = populate || Serviceuser.associations
        // .filter(ast => ast.autoPopulate !== false)
        // .map(ast => ast.alias)
        return buildQuery({
          model: Serviceuser,
          filters: { where: filters.where },
         // populate: populateOpt,
        });
    } 
};
