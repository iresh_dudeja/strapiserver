'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/services.html#core-services)
 * to customize this service
 */
// Public dependencies.
const _ = require('lodash');
const { convertRestQueryParams, buildQuery } = require('strapi-utils');

module.exports = {
     /**
   * Promise to fetch a/an Company.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    strapi.log.info("In a Company content type fetch service function: ",params);
    // Select field to populate.
    const populate = Company.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Company
      .findOne(_.pick(params, _.keys(Company.schema.paths)))
      .populate(populate);
  },
    /**
   * Promise to add a/an company.
   *
   * @return {Promise}
   */

  add: async (values) => {
    strapi.log.info("In a Company content type add service function: ",values);
    // Extract values related to relational data.
    const relations = _.pick(values, Company.associations.map(ast => ast.alias));
    const data = _.omit(values, Company.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Company.create(data);

    const result=await Company.updateRelations({ _id: entry.id, values: relations });
    //console.log("Result: ",result);
    return result;
  },

    /**
   * Promise to edit a/an Company.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    strapi.log.info("In a Company content type edit service function: ",params);
    // Extract values related to relational data.
    const relations = _.pick(values, Company.associations.map(a => a.alias));
    const data = _.omit(values, Company.associations.map(a => a.alias));

    // Update entry with no-relational data.
    const entry = await Company.updateOne({_id:params._id}, data, { multi: true });

    // Update relational data and return the entry.
    return Company.updateRelations(Object.assign(params, { values: relations }));
  },
   /**
   * Promise to remove a/an company.
   *
   * @return {Promise}
   */

  remove: async params => {
          strapi.log.info("In a Company content type remove service function: ",params);
          // Select field to populate.
          const populate = Company.associations
          .filter(ast => ast.autoPopulate !== false)
          .map(ast => ast.alias)
          .join(' ');

        // Note: To get the full response of Mongo, use the `remove()` method
        // or add spent the parameter `{ passRawResult: true }` as second argument.
        const data = await Company
          .findOneAndRemove({_id:params._id}, {})
          .populate(populate);
        
        if (!data) {
          return data;
        }

        await Promise.all(
          Company.associations.map(async association => {
            if (!association.via || !data._id || association.dominant) {
              return true;
            }

            const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
            const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

            // Retrieve model.
            const model = association.plugin ?
              strapi.plugins[association.plugin].models[association.model || association.collection] :
              strapi.models[association.model || association.collection];

            return model.update(search, update, { multi: true });
          })
        );

        return data;
   }

};
