'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
      /**
   * Retrieve a Company record.
   *
   * @return {Object}
   */

  findOneID: async (ctx) => {
    strapi.log.info("In a Company content type findOneID controller function: ",ctx.request);
    if(ctx.request.body["id"]==="undefined"){
        return ctx.notFound();
    }
    ctx.params._id=ctx.request.body["id"];
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.company.fetch(ctx.params);
  },

   /**
   * Create a/an Company record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    strapi.log.info("In a Company content type create controller function: ",ctx.request);
    return strapi.services.company.add(ctx.request.body);
   
    // return new Promise(result=>{
    //   const returnval= strapi.services.company.add(ctx.request.body).then(token => { return result({"Success":token}); } );
    
    //   if(returnval==="undefined" || returnval===null){
    //     return ctx.notFound();
    //   }
    
    // });
  },


  /**
   * Update a/an Company record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    strapi.log.info("In a Company content type update controller function: ",ctx.request);
    return strapi.services.company.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an Company record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    strapi.log.info("In a Company content type destroy controller function: ",ctx.request);
    return strapi.services.company.remove(ctx.params);
  }
  
};

