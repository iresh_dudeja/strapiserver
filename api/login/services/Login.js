'use strict';

/* global Login */

/**
 * Login.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');
const { convertRestQueryParams, buildQuery } = require('strapi-utils');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');
global.navigator = () => null;
//const config=require('../config/AWScredentials.json');
const config=require('../../../config/environments/development/AWScredentials.json');
const poolData = {
    UserPoolId: config["UserPoolId"],
    ClientId: config["ClientId"]
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);



module.exports = {

  /**
   * Promise to fetch all logins.
   *
   * @return {Promise}
   */

  fetchAll: (params, populate) => {
    strapi.log.info("In a Login content type fetchAll service function: ",params);
    const filters = convertRestQueryParams(params);
    const populateOpt = populate || Login.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)

    return buildQuery({
      model: Login,
      filters,
      populate: populateOpt,
    });
  },

  /**
   * Promise to fetch a/an login.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    strapi.log.info("In a Login content type fetch service function: ",params);
    // Select field to populate.
    const populate = Login.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Login
      .findOne(_.pick(params, _.keys(Login.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count logins.
   *
   * @return {Promise}
   */

  count: (params) => {
    strapi.log.info("In a Login content type count service function: ",params);
    const filters = convertRestQueryParams(params);

    return buildQuery({
      model: Login,
      filters: { where: filters.where },
    })
      .count()
  },

  /**
   * Promise to add a/an login.
   *
   * @return {Promise}
   */

  add: async (values) => {
    strapi.log.info("In a Login content type add service function: ",values);
    // Extract values related to relational data.
    const relations = _.pick(values, Login.associations.map(ast => ast.alias));
    const data = _.omit(values, Login.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Login.create(data);

    // Create relational data and return the entry.
    return Login.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an login.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    strapi.log.info("In a Login content type edit service function: ",params);
    // Extract values related to relational data.
    const relations = _.pick(values, Login.associations.map(a => a.alias));
    const data = _.omit(values, Login.associations.map(a => a.alias));
   
    // Update entry with no-relational data.
    const entry = await Login.updateOne(params, data, { multi: true });

    // Update relational data and return the entry.
    return Login.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an login.
   *
   * @return {Promise}
   */

  remove: async params => {
    strapi.log.info("In a Login content type remove service function: ",params);
    // Select field to populate.
    const populate = Login.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Login
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Login.associations.map(async association => {
        if (!association.via || !data._id || association.dominant) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an login.
   *
   * @return {Promise}
   */

  search: async (params) => {
    strapi.log.info("In a Login content type search service function: ",params);
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('login', params);
    // Select field to populate.
    const populate = Login.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Login.attributes).reduce((acc, curr) => {
      switch (Login.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Login
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  Login: async(body,callback)=>{
    strapi.log.info("In a Login content type Login service function: ",body);
      var userName = body.name;
      var password = body.password;
     // console.log("Username : "+userName+"password "+password);
     
       var cognitoUser = new AmazonCognitoIdentity.CognitoUser({
          Username: userName,
          Pool: userPool
      });
       cognitoUser.authenticateUser(new AmazonCognitoIdentity.AuthenticationDetails({
          Username: userName,
          Password: password
      }), {
           onSuccess: function (result) {
              var accesstoken = result.getAccessToken().getJwtToken();
              callback(null, accesstoken);
           },
           onFailure: (function (err) {
             strapi.log.error("Error In a Login content type Login service function: ",err);
              callback(err);
            
          })
      })
  
    },

    sendCode: async(body,callback)=>{
      strapi.log.info("In a Login content type sendCode service function: ",body);
      var userName = body.name;
      strapi.log.debug("Username/Email to send the code is: "+userName);
      var cognitoUser = new AmazonCognitoIdentity.CognitoUser({
          Username: userName,
          Pool: userPool
      });
  
      cognitoUser.forgotPassword({
          onSuccess: function (result) {
               callback(null);
          },
          onFailure: function(err) {
               strapi.log.error("Error In a Login content type sendCode service function: ",err);
              callback(err);
          },
         
      });
    },
    confirmCode: async(body,callback)=>{
      strapi.log.info("In a Login content type confirmCode service function: ",body);
      var cognitoUser=new AmazonCognitoIdentity.CognitoUser({
          Username:body.name,
          Pool:userPool
      });
      const verificationCode=body.code;
      const newPassword=body.newPassword;
      strapi.log.info("Verification code received: ",verificationCode);
      return new Promise((err, result) => {
          cognitoUser.confirmPassword(verificationCode, newPassword, {
              onFailure(err) {
                strapi.log.error("Error In a Login content type confirmcode service function: ",err);
                  callback(err);
              },
              onSuccess() {
                  callback(null,result);
              },
          });
      });
  },
  register: async(body,callback) => {
    strapi.log.info("In a Login content type register service function: ",body);
    const otherEmail=body.otherEmail;
    const companyName=body.cname;
    const cid=body.cid;

    const contactNo=body.phno;
    const username=body.name;
    const password=body.password;

    //console.log("Username : "+username+"password "+password);
    var attributeList = [];
   
    attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"email",Value:username}));
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser({
      Username: username,
      Pool: userPool
    });

    userPool.signUp(username,password, attributeList, null, function(err, result){
        if (err) {
            strapi.log.error("Error In a Login content type register service function: ",err);
            callback(err);
        }
        if(result){
          cognitoUser = result.user;
        }
        callback(null, cognitoUser);
    });
  }
};
