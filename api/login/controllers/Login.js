'use strict';

/**
 * Login.js controller
 *
 * @description: A set of functions called "actions" for managing `Login`.
 */

module.exports = {

  /**
   * Retrieve login records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    strapi.log.info("In a Login content type find controller function: ",ctx.request);
    if (ctx.query._q) {
      return strapi.services.login.search(ctx.query);
    } else {
      return strapi.services.login.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a login record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    strapi.log.info("In a Login content type findOne controller function: ",ctx.request);
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.login.fetch(ctx.params);
  },

  /**
   * Count login records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    strapi.log.info("In a Login content type count controller function: ",ctx.request);
    return strapi.services.login.count(ctx.query);
  },

  /**
   * Create a/an login record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    strapi.log.info("In a Login content type create controller function: ",ctx.request);
    return  new Promise(auth => {
      setTimeout(() => {
        strapi.services.login.Login(ctx.request.body,function(err,result){
                  if (err){
                      strapi.log.error("Error in a Login content type  create controller function: ",err);
                       auth({"error" :err});
                       return handleErrors(ctx, err['message'], 'unauthorized');
                      
                    }
                  auth({"token":result,"username":ctx.request.body["name"]});
              })
        }, 2000);
    });
  },

  sendCode: async(ctx) =>{
    strapi.log.info("In a Login content type sendCode controller function: ",ctx.request);
    return new Promise(auth=>{
      strapi.services.login.sendCode(ctx.request.body,function(err,result){
          if(err){
            strapi.log.error("Error in a Login content type  sendCode controller function: ",err);
          //  ctx.unathorized(err);
            auth({"error":err});
            return handleErrors(ctx, err['message'], 'unauthorized');
          }
          auth({"result":"success"});
      });
   });
  },

  confirmCode:async(ctx)=>{
    strapi.log.info("In a Login content type confirmCode controller function: ",ctx.request);
    return new Promise(auth=>{
        strapi.services.login.confirmCode(ctx.request.body,function(err,result){
          if(err){
            strapi.log.error("Error in a Login content type  confirmCode controller function: ",err);
            auth({"error":err});
            return handleErrors(ctx, err['message'], 'unauthorized');
          }
          auth({"result":"success"});
        });
    });

  },

  register:async(ctx)=>{
    strapi.log.info("In a Login content type register controller function: ",ctx.request);
    return new Promise(value=>{
      strapi.services.login.register(ctx.request.body,function(err,result){
          if(err){
            strapi.log.error("Error in a Login content type  register controller function: ",err);
            value({"error":err});
            return handleErrors(ctx, err['message'], 'unauthorized');
            //ctx.unauthorized(err);
          }
          value({"result":"success"});
      });
    });

  },
  /**
   * Update a/an login record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    strapi.log.info("In a Login content type update controller function: ",ctx.request);
    return strapi.services.login.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an login record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    strapi.log.info("In a Login content type destroy controller function: ",ctx.request);
    return strapi.services.login.remove(ctx.params);
  }
};
const handleErrors = (ctx, err = undefined, type) => {
  if (ctx.request.graphql === null) {
    return (ctx.request.graphql = strapi.errors[type](err));
  }

  return ctx[type](err);
};
