'use strict';

/**
 * Read the documentation () to implement custom service functions
 */

const _ = require('lodash');
const { convertRestQueryParams, buildQuery } = require('strapi-utils');
const config=require('../../../config/environments/development/AWScredentials.json');
const https = require('https');
const AWS = require('aws-sdk');
global.fetch = require('node-fetch');

const sessionTimeout = 100;

  
    
module.exports = {
     /**
   * Promise to fetch a/an Servicebooking DashboardURL record from local database.
   *
   * @return {Promise}
   */

  fetchURL: async(params,callback) => {
    strapi.log.info("In a Servicebooking content type fetchURL service function: ",params);
     const filters = convertRestQueryParams(params);
   //  console.log("params: ",params);
     const result= await buildQuery({
       model: Servicebooking,
       filters: filters,
     });
     //console.log("Result is: ",result);
      callback(null,result[0]);
  },


 /**
   * Create a/an login record.
   *
   * @return {Object}
   */
    fetchEmbeddedURL:async(params,callback)=>{
        strapi.log.info("In a Servicebooking content type fetchEmbeddedURL service function: ",params);
        const sts = new AWS.STS({ region:"us-east-1" });
        const params2 = {
            RoleArn: 'arn:aws:iam::868503999740:role/4fo-liebl-embed-dashboard-url',
            RoleSessionName: 'Strapisession',
            DurationSeconds: 3600,
        };

        const assumeRoleStep1 = await sts.assumeRole(params2,function (err, data) {
            if (err) {
                strapi.log.error("Error in  assumeRole fetchEmbeddedURL service function: ",err, err.stack);
                callback(err);
            } // an error occurred
          }).promise();
      
        AWS.config.update({
            accessKeyId: assumeRoleStep1.Credentials.AccessKeyId,
            secretAccessKey: assumeRoleStep1.Credentials.SecretAccessKey,
            sessionToken: assumeRoleStep1.Credentials.SessionToken
        });
        //console.log("Updated config: ",AWS.config);
        const qs = new AWS.QuickSight({region: "eu-central-1"});
        await qs.getDashboardEmbedUrl(
                    {
                        "AwsAccountId" : "868503999740",
                        "DashboardId" : "05c81e65-a4e9-4d5f-b14b-ee2986fca18a",
                        "IdentityType" : "IAM",
                        "ResetDisabled" : false,
                        "SessionLifetimeInMinutes" : sessionTimeout,
                        "UndoRedoDisabled" : false
                    },
                    (error, data) => {
                        if (error) {
                            strapi.log.error("Error in getting Quicksigh dashboard URL: "+error);
                        } else {
                            if (data.Status == 200) {
                               strapi.log.debug("Dashboard URL received: "+data.EmbedUrl);
                                //return (data.EmbedUrl);
                                callback(null,data.EmbedUrl);
                            } else {
                                strapi.log.error("Error in getting Quicksigh dashboard URL: " + data.Status);
                                callback(new Error("Server Response HTTP Status Code: " + data.Status));
                            }
                        }
                    }
             );       
        // callback(new Error("Server Response HTTP Status Code: "));    
        // callback(null,"xyz2.com"); 
    },
    /**
   * Create a/an DashboardURL to database.
   *
   * @return {Object}
   */   
  addEmbeddedURL:async(params)=>{
        strapi.log.info("In a Servicebooking content type addEmbeddedURL service function: ",params);
        strapi.log.info("Company ID to update is: ",params.Cid);
        const result=await Servicebooking.updateOne({"Cid":params.Cid},{DashboardURL:params.DashboardURL},{ multi: false }); 
        strapi.log.info("Udpadated Servicebooking dashboardURL record: ",result);
        return result;
    }
};

