'use strict';

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {


/**
   * Get a Quicksight dashboard URL for given servicebooking from local database.
   *
   * @return {Object}
   */

  getdbURL: async (ctx, next) => {
    return new Promise((resolve,reject)=>{
      // console.log("Strapi context: ",ctx);
        strapi.log.info("In a Servicebooking content type getdbURL controller function: ",ctx.request);
        ctx.params.Cid=ctx.request.user.company.toString();
        strapi.log.info("Company ID: ",ctx.request.user);
        strapi.log.info("Company ID is set for a given request : , ",ctx.params.Cid);
        
        strapi.services.servicebooking.fetchURL({"Cid":ctx.params.Cid},function(err,res){
          strapi.log.info("Return fetchURL value: ",res);
          if(typeof(res)==undefined || res==null ||res===""){
              strapi.log.error("Dashboard URL not found in database!");
              reject(new Error("Dashboard URL not found in database!"));
              handleErrors(ctx, err['message'], 'unauthorized');
            
          }
          resolve({"DashboardURL":res.DashboardURL});
        });
        
   }, 4000);  
  },



 /**
   * Get a Quicksight dashboard URL for given servicebooking.
   *
   * @return {Object}
   */

    getURL:async(ctx,next)=>{
      
            return new Promise((resolve,reject)=>{
              //  setTimeout(() => {
                strapi.log.info("In a Servicebooking content type getURL controller function: ",ctx.request);
                ctx.params.Cid=ctx.request.user.company.toString();
                strapi.log.info("Company ID: ",ctx.request.user);
                strapi.log.info("Company ID is set for a given request : , ",ctx.params.Cid);

                      strapi.services.servicebooking.fetchEmbeddedURL(ctx.params,function(err,url){
                        if(err){
                                      strapi.log.error("Error in a Servicebooking content type getURL controller function: ",err);
                                      reject(new Error("Dashboard URL not found!"));
                                      return; 
                                } else{
                                  if(url !=="" && url !==undefined && url !==null){
                                    strapi.services.servicebooking.addEmbeddedURL({"Cid":ctx.params.Cid, "DashboardURL":url}).then(res=>{console.log("Added URL to database!");})
                                    resolve({"DashboardURL":url}); 
                                  }
                                  
                                }        
                 // }); 
                 // console.log("Some problem reached here !");    
            });
        }, 6000);  
    }
};

const handleErrors = (ctx, err = undefined, type) => {
    if (ctx.request.graphql === null) {
      return (ctx.request.graphql = strapi.errors[type](err));
    }
    return ctx[type](err);
  };
  