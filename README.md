# Strapi application

The management console will serve as a central dashboard to manage, purchase or explore different services provided by 4fo.de . Overall functionalities can be listed below:
    1.  Centralize dashboard  for all available services
    2.  Billing service based on no. of purchased services
    3.  User login  management to this dashboards

Following are the steps for the project Setup:

1. Install MongoDB database locally. https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/#install-mdb-edition

2. Edit host and port of the mongodb in config/environment/development/database.json  file

3. Generate yarn.lock file with command: yarn

4. Start strapi server using command: yarn develop

5. To access the server visit: http://localhost:1336

